# mas-atd-evaluation

R scripts and data to replicate the study named "Architectural Technical Debt of Multiagent Systems Development Platforms" presented at WOA 2021.

## Getting started

This folder contains the necessary raw data to replicate the work “Architectural Technical Debt of Multi-Agent Systems Development Platforms”.

- **./arcan-output-github** contains the raw data uset to build the dataset (the Arcan output files). The graphml files contains the Arcan dependency graph and can be open by graph editors such as [YeD](https://www.yworks.com/products/yed). The csv files contains the data about AS and ADI.
- **./Dataset** contains the data used for the statistical analysis, one file per project. Each row corresponds to a unique smell. The dataset must be grouped by commit in order to apply the Mann-Kendall test. The important column to use for the analysis are:
    - AS: the type of architectural smell (UD, HL, CD)
    - ADI:  the value of the ATD index
    - project: the name of the project under analysis
    - version: the incremental counter of the commits
    - arcan.version: the counter of the sampled commits
    - commit.hash: the hash of the commit (as given by Github)

- **./scripts** contains the R scripts necessary to build the dataset (`create_project_dataframe.R`), to run the analysis (`analysis.R`) and to create the plots (`plots.R`)

## Contributor
Ilaria Pigazzini

University of Milano - Bicocca

email: ilaria.pigazzini@unimib.it



